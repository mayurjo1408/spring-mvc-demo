<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer confirmation</title>
</head>
<body>
	The customer is confirmed ${customer.strFirstName}
	${customer.strLastName}
	<br>
	<br> Free passes: ${customer.intFreePasses}
	<br>
	<br> Postal Code: ${customer.strPostalCode}
	<br>
	<br> Course Code: ${customer.strCourseCode}
</body>
</html>