<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/main.css">
<title>Customer Registration Form</title>
</head>
<body bgcolor="white">
	Fill out the form. Asterisk (*) means required.
	<br>
	<br>
	<form:form action="processForm" modelAttribute="customer">
	First Name: <form:input path="strFirstName" />
		<br>
		<br>
	Last Name (*): <form:input path="strLastName" />
		<form:errors path="strLastName" cssClass="error" />
		<br>
		<br>
	Free Passes: <form:input path="intFreePasses" />
		<form:errors path="intFreePasses" cssClass="error" />
		<br>
		<br>
	Postal Code: <form:input path="strPostalCode" />
		<form:errors path="strPostalCode" cssClass="error" />
		<br>
		<br>
	Course Code: <form:input path="strCourseCode" />
		<form:errors path="strCourseCode" cssClass="error" />
		<br>
		<br>
		<input type="submit" value="Submit" />
	</form:form>
</body>
</html>