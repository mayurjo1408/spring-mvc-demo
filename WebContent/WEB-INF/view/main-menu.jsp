<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Main Menu</title>
</head>
<body>
	<h2>Welcome to Spring MVC!</h2>
	<hr>
	<a href="showForm">Display Name</a> |
	<a href="calculator/showCalculatorPage">Calculator</a> |
	<a href="student/showForm">Student Form</a> |
	<a href="customer/showForm">Customer Form</a>
</body>
</html>