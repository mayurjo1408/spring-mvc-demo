<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Registration Form</title>
</head>
<body>
	<form:form action="processForm" modelAttribute="student">
	First Name: <form:input path="strFirstName" />
		<br>
		<br>
	Last Name: <form:input path="strLastName" />
		<br>
		<br>
	Country: <form:select path="strCountry">

			<!-- Static data method -->
			<%-- <form:option value="Brazil" label="Brazil"></form:option>
			<form:option value="France" label="France"></form:option>
			<form:option value="Germany" label="Germany"></form:option>
			<form:option value="India" label="India"></form:option> --%>

			<!-- Dynamic data method -->
			<form:options items="${student.lhmCountries}" />

		</form:select>
		<br>
		<br>
		Favourite subject:
		Java <form:radiobutton path="strFavSubject" value="Java" />
		C# <form:radiobutton path="strFavSubject" value="C#" />
		PHP <form:radiobutton path="strFavSubject" value="PHP" />
		Python <form:radiobutton path="strFavSubject" value="Python" />
		<br>
		<br>
		Operating Systems:
		Linux <form:checkbox path="straOperatingSystems" value="Linux" />
		Mac OS <form:checkbox path="straOperatingSystems" value="Mac OS" />
		MS Windows <form:checkbox path="straOperatingSystems"
			value="MS Windows" />
		<br>
		<br>
		<input type="submit" value="Submit">
	</form:form>
</body>
</html>