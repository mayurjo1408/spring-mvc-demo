<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Registration Confirmation</title>
</head>
<body>
	The student is confirmed: ${student.strFirstName}
	${student.strLastName}
	<br>
	<br> Country: ${student.strCountry}
	<br>
	<br> Favorite Subject: ${student.strFavSubject}
	<br>
	<br> Operating Systems:
	<ul>
		<c:forEach var="os" items="${student.straOperatingSystems}">
			<li>${os}</li>
		</c:forEach>
	</ul>
</body>
</html>