package com.springpractice.mvcdemo;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/customer")
public class CustomerController {

	// Add an initBinder to trim String input to controller
	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
		StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);

		webDataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
	}

	@RequestMapping("/showForm")
	public String showForm(Model mdlCustomer) {
		mdlCustomer.addAttribute("customer", new Customer());

		return "customer-form";
	}

	@RequestMapping("/processForm")
	public String processForm(@Valid @ModelAttribute("customer") Customer customer, BindingResult bindingResult) {
		System.out.println("First Name: " + customer.getStrFirstName());
		System.out.println("Last Name: " + customer.getStrLastName());
		System.out.println("Free Passes: " + customer.getIntFreePasses());
		System.out.println("Postal Code: " + customer.getStrPostalCode());

		System.out.println("Binding Result: " + bindingResult);

		if (bindingResult.hasErrors())
			return "customer-form";
		else
			return "customer-confirmation";
	}
}
