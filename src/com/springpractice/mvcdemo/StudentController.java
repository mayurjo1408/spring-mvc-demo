package com.springpractice.mvcdemo;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
public class StudentController {

	@RequestMapping("/showForm")
	public String showForm(Model mdlStudent) {
		// Add Student object to the model
		mdlStudent.addAttribute("student", new Student());

		return "student-form";
	}

	@RequestMapping("/processForm")
	public String processForm(@ModelAttribute("student") Student student) {
		System.out.println("First Name: " + student.getStrFirstName());
		System.out.println("Last Name: " + student.getStrLastName());
		System.out.println("Country: " + student.getStrCountry());
		System.out.println("Favourite subject: " + student.getStrFavSubject());

		System.out.println("Operating Systems: ");
		for (String strOS : student.getStraOperatingSystems()) {
			System.out.println(strOS);
		}

		return "student-confirmation";
	}
}
