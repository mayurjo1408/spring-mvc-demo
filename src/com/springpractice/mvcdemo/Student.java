package com.springpractice.mvcdemo;

import java.util.LinkedHashMap;

public class Student {
	private String[] straOperatingSystems;
	private String strFirstName, strLastName, strCountry, strFavSubject;
	private LinkedHashMap<String, String> lhmCountries;

	public Student() {
		lhmCountries = new LinkedHashMap<>();

		lhmCountries.put("BR", "Brazil");
		lhmCountries.put("FR", "France");
		lhmCountries.put("DE", "Germany");
		lhmCountries.put("IN", "India");
		lhmCountries.put("CN", "Canada");
	}

	public String getStrFirstName() {
		return strFirstName;
	}

	public void setStrFirstName(String strFirstName) {
		this.strFirstName = strFirstName;
	}

	public String getStrLastName() {
		return strLastName;
	}

	public void setStrLastName(String strLAstName) {
		this.strLastName = strLAstName;
	}

	public String getStrCountry() {
		return strCountry;
	}

	public void setStrCountry(String strCountry) {
		this.strCountry = strCountry;
	}

	public LinkedHashMap<String, String> getLhmCountries() {
		return lhmCountries;
	}

	public void setLhmCountries(LinkedHashMap<String, String> lhmCountries) {
		this.lhmCountries = lhmCountries;
	}

	public String getStrFavSubject() {
		return strFavSubject;
	}

	public void setStrFavSubject(String strFavSubject) {
		this.strFavSubject = strFavSubject;
	}

	public String[] getStraOperatingSystems() {
		return straOperatingSystems;
	}

	public void setStraOperatingSystems(String[] straOperatingSystems) {
		this.straOperatingSystems = straOperatingSystems;
	}
}
