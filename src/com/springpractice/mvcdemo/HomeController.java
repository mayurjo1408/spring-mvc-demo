package com.springpractice.mvcdemo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
	// Controller method to show initial form
	@RequestMapping("/")
	public String showMenuPage() {
		return "main-menu";
	}
}
