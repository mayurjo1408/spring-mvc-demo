package com.springpractice.mvcdemo.validation;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = CourseCodeValidator.class)
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention(RUNTIME)
public @interface CourseCode {
	// Define default course code
	public String value() default "LUV";

	// Define default error message
	public String message() default "must start with 'LUV'";

	// Define default groups
	public Class<?>[] groups() default {};

	// Default default payloads
	public Class<? extends Payload>[] payload() default {};
}
