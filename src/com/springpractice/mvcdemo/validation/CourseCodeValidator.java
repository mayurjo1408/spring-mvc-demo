package com.springpractice.mvcdemo.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CourseCodeValidator implements ConstraintValidator<CourseCode, String> {

	private String strCoursePrefix;

	@Override
	public void initialize(CourseCode courseCode) {
		strCoursePrefix = courseCode.value();
	}

	@Override
	public boolean isValid(String CourseCode, ConstraintValidatorContext constraintValidatorContext) {
		boolean boolResult;

		if (CourseCode != null)
			boolResult = CourseCode.startsWith(strCoursePrefix);
		else
			boolResult = true;

		return boolResult;
	}

}
