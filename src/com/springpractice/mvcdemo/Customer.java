package com.springpractice.mvcdemo;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.springpractice.mvcdemo.validation.CourseCode;

public class Customer {
	private String strFirstName;

	@NotNull(message = "is required.")
	@Size(min = 1, message = "is required.")
	private String strLastName;

	@Pattern(regexp = "^[a-zA-Z0-9]{5}", message = "only 5 chars/digits")
	private String strPostalCode;

	@CourseCode
	private String strCourseCode;

	@NotNull(message = "is required.")
	@Min(value = 0, message = "must be greater than or equal to 0.")
	@Max(value = 10, message = "must be less than or equal to 10.")
	private Integer intFreePasses;
//	private int intFreePasses; Commented this and added new line to handle integer parsing error on the form

	public String getStrFirstName() {
		return strFirstName;
	}

	public void setStrFirstName(String strFirstName) {
		this.strFirstName = strFirstName;
	}

	public String getStrLastName() {
		return strLastName;
	}

	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}

	public Integer getIntFreePasses() {
		return intFreePasses;
	}

	public void setIntFreePasses(Integer intFreePasses) {
		this.intFreePasses = intFreePasses;
	}

	public String getStrPostalCode() {
		return strPostalCode;
	}

	public void setStrPostalCode(String strPostalCode) {
		this.strPostalCode = strPostalCode;
	}

	public String getStrCourseCode() {
		return strCourseCode;
	}

	public void setStrCourseCode(String strCourseCode) {
		this.strCourseCode = strCourseCode;
	}
}
