package com.springpractice.mvcdemo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloWorldController {
	// Controller method to show initial form
	@RequestMapping("/showForm")
	public String showForm() {
		return "helloworld-form";
	}

	// Controller method to process initial form
	@RequestMapping("/processForm")
	public String processForm() {
		return "helloworld";
	}
}
