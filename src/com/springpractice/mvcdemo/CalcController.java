package com.springpractice.mvcdemo;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/calculator")
public class CalcController {
	@RequestMapping("/showCalculatorPage")
	public String showCalculatorPage() {
		return "calculator";
	}

//	@RequestMapping("/showResult")
	public String showResultPageV1(HttpServletRequest request, Model model) {
		int intNum1 = Integer.parseInt(request.getParameter("txtNum1"));
		int intNum2 = Integer.parseInt(request.getParameter("txtNum2"));

		int intResult = intNum1 + intNum2;
		model.addAttribute("result", intResult);

		return "resultpage";
	}

	@RequestMapping("/showResult")
	public String showResultPageV2(@RequestParam("txtNum1") int intNum1, @RequestParam("txtNum2") int intNum2,
			Model model) {
		System.out.println("From V2.");

		int intResult = intNum1 + intNum2;
		model.addAttribute("result", intResult);

		return "resultpage";
	}
}
